﻿using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngameScript
{
    internal class NTPDConfiguration : BaseConfiguration
    {

        private static int DEFAULT_DAY_DURATION = 120;
        private static int DEFAULT_YEAR_OFFSET = -22000;

        private static string DAY_DURATION_KEY = "day-duration";
        private static string YEAR_OFFSET_KEY = "year-offset";

        public NTPDConfiguration(IMyTerminalBlock block) : base(block)
        { }

        protected override Dictionary<string, object> defaultConfiguration()
        {
            var defaults = base.defaultConfiguration();
            defaults.Add(DAY_DURATION_KEY, DEFAULT_DAY_DURATION);
            defaults.Add(YEAR_OFFSET_KEY, DEFAULT_YEAR_OFFSET);
            return defaults;
        }

        public int getDayDuration()
        {
            return ini.Get(SECTION, DAY_DURATION_KEY).ToInt32();
        }

        public long getYearOffset()
        {
            return ini.Get(SECTION, YEAR_OFFSET_KEY).ToInt64();
        }
    }
}
