﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {

        private string broadcastChannel;
        private long yearOffset;
        private int factor;

        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update100;
            initialize();
        }

        public void initialize()
        {
            var configuration = new NTPDConfiguration(Me);

            this.factor = (60 * 24) / configuration.getDayDuration();
            this.yearOffset = configuration.getYearOffset() * 1000 * 60 * 60 * 24 * 365;
            this.broadcastChannel = configuration.getChannel();
        }

        public void Main(string argument, UpdateType updateSource)
        {
            var realTimeMilliseconds = ((long)(DateTime.UtcNow - DateTime.MinValue).TotalMilliseconds);
            var ingameMilliseconds = realTimeMilliseconds * factor;

            ingameMilliseconds += yearOffset;

            IGC.SendBroadcastMessage(broadcastChannel, ingameMilliseconds);
            Echo("sending time data " + DateObject.toDateTime(ingameMilliseconds));
        }
    }
}
